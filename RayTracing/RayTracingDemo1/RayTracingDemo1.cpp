#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <algorithm>

struct Vector
{
	double x, y, z;

	Vector(double x = 0, double y = 0, double z = 0)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	Vector operator+(const Vector &b) const
	{
		return Vector(x + b.x, y + b.y, z + b.z);
	}

	Vector operator-(const Vector &b) const
	{
		return Vector(x - b.x, y - b.y, z - b.z);
	}

	Vector operator*(double b) const
	{
		return Vector(x*b, y*b, z*b);
	}
};

int main()
{
	return 0;
}

